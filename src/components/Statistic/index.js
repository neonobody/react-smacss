import React, { Component } from 'react'
import SmoothieComponent from 'react-smoothie';
export default class Statistic extends Component {
    componentDidMount() {
        let ts1 = this.refs.stat1.addTimeSeries({},{ strokeStyle: '#17c84a', fillStyle: '#17c84a', lineWidth: 1 });
        let ts2 = this.refs.stat2.addTimeSeries({},{ strokeStyle: '#2d69e9', fillStyle: '#2d69e9', lineWidth: 1 });
        this.dataGenerator = setInterval(function() {
            let time = new Date().getTime();
            ts1.append(time, Math.random());
            ts2.append(time, Math.random());
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.dataGenerator);
    }
    render() {
        return (
            <div>
                <SmoothieComponent grid={{
                    strokeStyle: '#eee',
                    fillStyle: '#eee',
                    lineWidth: 1,
                    millisPerLine: 200,
                    verticalSections: 1
                }} labels={{fillStyle: '#000'}} ref="stat1" width="119" height="27"/>
                <SmoothieComponent grid={{
                    strokeStyle: '#eee',
                    fillStyle: '#eee',
                    lineWidth: 1,
                    millisPerLine: 200,
                    verticalSections: 1
                }} labels={{fillStyle: '#000'}} ref="stat2" width="119" height="27"/>
            </div>
        )
    }
}
