import React, { Component } from 'react'
// import Statistic from '../../components/Statistic'
import {ProgressBar} from 'react-bootstrap';
import './style.scss';
export default class Instance extends Component {

    render() {
        return (
            <div className="col-md-10 instance">
                <ul>
                    {this.props.info.instances ? this.props.info.instances.map(function(item){
                        return(

                            <li key = {item.id} >
                                <div className="instance_name">
                                    <h2>{item.name}</h2>
                                    <p>12ah3.someLink.io</p>
                                </div>
                                <div className="instance_statistic" ref="stat01">
                                    <p className="storage-info">{item.size} / {this.props.info.maxMemoryLimit} GB</p>
                                    <ProgressBar className="progress_simple" now={100/(this.props.info.maxMemoryLimit/item.size)}/>
                                    {/*<Statistic/>*/}
                                </div>
                                <div className="instance_control">
                                    <button className="edit">Edit</button>
                                    <button className="remove" onClick={this.props.remove}>Remove</button>
                                </div>

                            </li>
                        )

                    }, this) : null}
                </ul>
            </div>
        )
    }
}