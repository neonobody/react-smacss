import React, { Component } from 'react'
import {Button, Form, FormGroup, Col, ControlLabel, FormControl} from 'react-bootstrap';
import Slider from 'rc-slider';
import './style.scss';
export default class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            value: Math.round(this.props.defaultSettings.maxMemoryLimit / this.props.defaultSettings.instanceLimit)
        })

    }
    resetSlider(){
        this.setState({
            value: Math.round(this.props.defaultSettings.maxMemoryLimit / this.props.defaultSettings.instanceLimit)
        });
    }
    onSliderChange(value) {
        this.setState({
            ...this.state, value: value
        });
    }
    onInputChange(e) {
        this.setState({
            ...this.state, value: e.target.value
        });
    }
    render() {
        return (
            <div className="col-md-6 col-sm-8">
                <div className="row">
                    <div className="col-md-12 col-sm-12 settings">
                        <div className="area">
                            <h2>New instance</h2>
                            <div className="range">
                                <span className="pull-left">{this.props.defaultSettings.minMemoryLimit} GB</span>
                                <span className="mid">{this.props.defaultSettings.maxMemoryLimit / 2} GB</span>
                                <span className="pull-right">{this.props.defaultSettings.maxMemoryLimit} GB</span>
                            </div>
                            <Slider  tipFormatter={null} onChange={::this.onSliderChange} min={this.props.defaultSettings.minMemoryLimit} max={this.props.defaultSettings.maxMemoryLimit} value={this.state.value} step={1} />
                            <Form horizontal onSubmit={this.props.create}>
                                <FormGroup controlId="instanceSize">
                                    <Col componentClass={ControlLabel}>
                                        Size:
                                    </Col>
                                    <FormControl type="number" inputmode="numeric" pattern="[0-9]*" className="sizeInput" min={this.props.defaultSettings.minMemoryLimit} max={this.props.defaultSettings.maxMemoryLimit} value={this.state.value} onChange={::this.onInputChange}/>
                                    <span>GB</span>
                                </FormGroup>

                                <FormGroup controlId="instanceBackups">
                                    <Col  componentClass={ControlLabel}>
                                        Backups:
                                    </Col>
                                    <label className="switch backupInput">
                                        <input type="checkbox" id="instanceBackups"/>
                                        <div className="slider round"></div>
                                    </label>

                                </FormGroup>

                                <FormGroup controlId="instanceName">
                                    <Col  componentClass={ControlLabel}>
                                        Name:
                                    </Col>
                                    <FormControl type="text" className="nameInput"/>
                                </FormGroup>
                                <Button onClick={::this.resetSlider} className="button" type="submit">
                                    Create
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

