import React, { Component } from 'react'
import './style.scss';
export default class Widget extends Component {

    render() {
        return (
            <div className="col-md-4 col-sm-8 widget">
                <div className="area">
                    <h2>Billing</h2>
                    <div className="widget__info">
                        <ul>
                            <li>
                                <span>Plan:</span>
                                <span>{this.props.info.plan}</span>
                            </li>
                            <li>
                                <span>Memory limit:</span>
                                <span>{this.props.count()} / {this.props.info.maxMemoryLimit} GB</span>
                            </li>
                            <li>
                                <span>Instance limit:</span>
                                <span>{this.props.info.instances.length} / {this.props.info.instanceLimit}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}