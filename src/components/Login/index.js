import React, { Component } from 'react'
import NavLink from '../../components/NavLink'
import {FormGroup, Form, FormControl, HelpBlock, Button, Col} from 'react-bootstrap';
export default class Login extends Component {
    render() {
        return (
            <Form onSubmit={this.props.send}>
                <h2 className="text-center">Log in</h2>

                {/*<HelpBlock>You can use your Social network account to Sign in</HelpBlock>*/}

                {/*<ButtonToolbar className="social-login">*/}
                    {/*<Button className="fb">Facebook</Button>*/}
                    {/*<Button className="g-plus">Google</Button>*/}
                    {/*<Button className="vk">Vkontakte</Button>*/}
                    {/*<Button className="tw">Twitter</Button>*/}
                {/*</ButtonToolbar>*/}
                <Col className="area area_fit">
                    <FormGroup controlId="logInName">
                        {/*<ControlLabel>Username</ControlLabel>*/}
                        <FormControl required type="text" placeholder="Username"/>
                    </FormGroup>
                    <FormGroup controlId="logInPass">
                        {/*<ControlLabel >Password</ControlLabel>*/}
                        <FormControl required type="password" placeholder="Password"/>
                    </FormGroup>
                </Col>

                <HelpBlock>Don’t have an account?
                    <NavLink>Sign up</NavLink>
                </HelpBlock>
                <Button className="button" type="submit">Log in</Button>
            </Form>
        )
    }
}
