import React, { Component } from 'react'
import {FormGroup, Form, FormControl, HelpBlock, Button, Col} from 'react-bootstrap';
import NavLink from '../../components/NavLink'
export default class Signup extends Component {

    render() {
        return (
            <Form className="signup" onSubmit={this.props.send}>
                <h2 className="text-center">Sign up</h2>
                <Col className="area area_fit">
                    <FormGroup controlId="signUpName">
                        {/*<ControlLabel>Username</ControlLabel>*/}
                        <FormControl required type="text" placeholder="Username"/>
                    </FormGroup>
                    <FormGroup controlId="signUpPass">
                        {/*<ControlLabel>Password</ControlLabel>*/}
                        <FormControl required type="password" placeholder="Password"/>
                    </FormGroup>
                    <FormGroup controlId="signUpConfirmPass">
                        {/*<ControlLabel>Confirm Password</ControlLabel>*/}
                        <FormControl required type="password" placeholder="Confirm Password"/>
                    </FormGroup>
                    <FormGroup controlId="signUpEmail">
                        {/*<ControlLabel>Email adress</ControlLabel>*/}
                        <FormControl required type="email" placeholder="Email adress"/>
                    </FormGroup>
                </Col>

                <HelpBlock>Already have an account?
                    <NavLink>Log in</NavLink>
                </HelpBlock>
                <Button className="button" type="submit">SIGN UP</Button>
            </Form>
        )
    }
}