import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import './style.scss';
export default class ProjectNav extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            setName: false,
            edit: null
        });
    }
    setName(){
        this.setState({...this.state, setName: true, edit: null})
    }
    toDefault(){
        this.setState({...this.state, setName: false, edit: null})
    }

    setProjectEdit(e){
        this.setState(
            {...this.state, setName: false, edit: e.target.parentNode.getAttribute("data-number")}
        )
    }

    changeProject(e){
        console.log(e.target.nodeName)
        if(this.refs.projectNav.classList.contains('open') && e.target.nodeName == "A"){
            this.refs.projectNav.classList.remove('open');
            document.getElementsByClassName('burger')[0].classList.remove('open');
        }
    }

    projectMenu() {
        document.getElementsByClassName('burger')[0].classList.add('open');
        this.refs.projectNav.classList.add('open');
    }

    componentDidMount() {
            this.refs.currentProject.appendChild(this.refs.active.cloneNode(true));
    }
    componentDidUpdate(){
        if(this.props.info.length != 0 && this.refs.active.nodeName == "A") {
            this.refs.currentProject.innerHTML = '';
            this.refs.currentProject.appendChild(this.refs.active.cloneNode(true));
        }
        if(this.refs.projectNav.querySelector('div')){
            this.refs.projectNav.querySelector('div').childNodes[0].focus()
        }
        {this.state.setName ? this.refs.changeName.focus() : null}

    }

    render() {
        return(
            <div className='col-md-2 col-sm-4 no-p-r sidebar'>
                <nav className="project-menu navbar navbar-default" >
                    <div className="container">
                        <span className="current-project" ref="currentProject" onClick={::this.projectMenu} />
                        <ul ref="projectNav" id="projectNav" className="nav">
                            {this.props.info.map(function(item, index){
                                return(
                                    this.state.edit != index ?
                                        <a key={item.id} data-number={index} data-name={item.name} ref={this.props.current == index ? "active" : null} className={this.props.current == index ? "currentProject" : null} onClick={(e) => {this.props.navigation(e); ::this.changeProject(e)}}>
                                            {item.name}
                                            <span className="glyphicon glyphicon-edit" onClick={::this.setProjectEdit}/>
                                        </a>
                                        :
                                        <div key={item.id} data-number={index} ref={this.props.current == index ? "active" : null}>
                                            <input type="text"  placeholder="New name" />
                                            <span className='glyphicon glyphicon-ok' onClick={(e) => {this.props.changeName(e), ::this.toDefault()}}/>
                                            <span className='glyphicon glyphicon-remove' onClick={::this.toDefault} />
                                            <span className='glyphicon glyphicon-trash' onClick={(e) => {this.props.trashProject(e); ::this.toDefault()}} />
                                        </div>
                                )
                            }, this)}

                            {this.state.setName
                                ?
                                <Button className="change-name" >
                                    <input type="text" ref="changeName" placeholder="Name"/>
                                    <span className='glyphicon glyphicon-ok' onClick={(e) => {this.props.addNew(e), ::this.toDefault()}}/>
                                    <span className='glyphicon glyphicon-remove' onClick={::this.toDefault} />
                                </Button>
                                :
                                <Button className="add" onClick={::this.setName}><span>+</span>Add new</Button>
                            }
                        </ul>

                    </div>
                </nav>
            </div>
        )
    }
}
