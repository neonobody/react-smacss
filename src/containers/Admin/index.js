import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as AdminActions from '../../actions/AdminActions'
import ProjectNav from '../../components/ProjectNav';
import Widget from '../../components/Widget';
import Instance from '../../components/Instance';
import Settings from '../../components/Settings';
import './style.scss';
export class Admin extends Component {
    addProject(e) {
        e.preventDefault()
        this.props.actions.addProject({name: e.target.previousSibling.value, count: this.props.admin.projects.length})
    }
    createInstance(e){
        e.preventDefault();
        this.props.actions.createInstance({size: e.target.elements.instanceSize.value, name: e.target.elements.instanceName.value, backups: e.target.elements.instanceBackups.checked})
        e.target.elements.instanceSize.value = '';
        e.target.elements.instanceName.value = '';
        e.target.elements.instanceBackups.checked = false;
    }

    projectNav(e){
        e.target.nodeName == "A" ? this.props.actions.changeProject({current: e.target.getAttribute('data-number')}) : null
    }

    countMemory(){
        let sum = 0, i;
        for (i = 0; i < this.props.admin.projects[this.props.admin.current].instances.length; i++){
            sum += Number(this.props.admin.projects[this.props.admin.current].instances[i].size);
        }
        return sum
    }
    removeInstance(e){
        let el = e.target.parentNode.parentNode.parentNode.childNodes,
            target = e.target.parentNode.parentNode;
        for(let i = 0; i < el.length; i++) {
            if(el[i] == target) {{
                this.props.actions.removeInstance({number: i})
                break
            }}
        }

    }

    changeProjectName(e){
        this.props.actions.editProject({name: e.target.parentNode.childNodes[0].value, number: e.target.parentNode.getAttribute("data-number")})
    }

    removeProject(e){
        this.props.actions.removeProject({number: e.target.parentNode.getAttribute("data-number")})
        if (this.props.admin.current == e.target.parentNode.getAttribute("data-number")){
            this.props.actions.changeProject({current: 0})
        }
    }

    render() {
        return (
            <div className="admin">
                <div className='container'>
                    <div className="row">
                        <ProjectNav
                            changeName={::this.changeProjectName}
                            current={this.props.admin.current}
                            info={this.props.admin.projects}
                            addNew={::this.addProject}
                            navigation={::this.projectNav}
                            trashProject={::this.removeProject}/>
                        {this.props.admin.projects.length != 0 ?
                            <div className="clearfix">
                                <Settings
                                    create={::this.createInstance}
                                    defaultSettings={this.props.admin.projects[this.props.admin.current]}/>
                                < Widget
                                    count={::this.countMemory}
                                    info={this.props.admin.projects[this.props.admin.current]} />
                                <Instance
                                    info={this.props.admin.projects[this.props.admin.current]}
                                    remove={::this.removeInstance} />
                            </div>
                        : null}





                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        admin: state.admin
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(AdminActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin)