import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as UserActions from '../../actions/UserActions'
import Header from '../Header'
import './style.scss';

export class App extends Component {
    render() {
        return (
            <div className="container-fluid">
                <Header isSignupPage={this.props.children.props.location.pathname == '/signup'}/>
                <div className='row'>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

function mapStateToProps() {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(UserActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)