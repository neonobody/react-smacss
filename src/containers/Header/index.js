import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as UserActions from '../../actions/UserActions'
import {Button, DropdownButton, MenuItem, ButtonToolbar} from 'react-bootstrap'
import { Link } from 'react-router'
import './style.scss'

export class Header extends Component {
    logOut(e) {
        e.preventDefault()
        this.props.actions.logout()
    }
    mobileMenu() {
        if(this.refs.burger.classList.contains('open')){
            this.refs.burger.classList.remove('open');
            document.getElementById('projectNav').classList.remove('open');
        } else{
            this.refs.header.classList.toggle('open');
        }
    }
    closeMobileMenu(e){
        console.log(e.target.tagName);
        if(e.target.tagName == "A"){
            this.refs.header.classList.remove('open');
        }
    }
    render() {
        return (
                <nav className="navbar navbar-fixed-top navbar-inverse header" onClick={::this.closeMobileMenu} ref="header">
                    <div className="container">
                        <div className="navbar-header">
                            <a className="logo" href="#"></a>
                        </div>
                        <div className="burger" ref="burger" onClick={::this.mobileMenu}><span></span></div>

                            {this.props.user.isAuthenticated ?
                                <ButtonToolbar>
                                    <DropdownButton className="log log_ed" title={this.props.user.name} id="dropdown-size-large">
                                        <MenuItem eventKey="1">Profile</MenuItem>
                                        <MenuItem eventKey="2">Billing</MenuItem>
                                        <MenuItem eventKey="3">Audit</MenuItem>
                                        <MenuItem eventKey="4" onClick={::this.logOut}>Log out</MenuItem>
                                    </DropdownButton>
                                </ButtonToolbar>


                                :
                                <ButtonToolbar>
                                    {/*buttons for non permanent users*/}
                                    {/*<Button className="log log_out" onClick={::this.logOut}>Log out</Button>*/}
                                    {/*<Button className="button permanent">MAKE THIS ACCOUNT PERMANENT</Button>*/}
                                    {this.props.isSignupPage ? <Button className="log log_in"><Link to='/login'>Log in</Link></Button> : null}
                                </ButtonToolbar>}
                            </div>



                </nav>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(UserActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)