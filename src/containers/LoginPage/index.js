import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as UserActions from '../../actions/UserActions'
import Login from '../../components/Login'
import Signup from '../../components/Signup'
import './style.scss';

export class LoginPage extends Component {
  logIn(e) {
    e.preventDefault()
    this.props.actions.login({name: e.target.elements.logInName.value, pass: e.target.elements.logInPass.value});
  }
  signUp(e) {
      e.preventDefault()
      this.props.actions.login({name: e.target.elements.signUpName.value, pass: e.target.elements.signUpPass.value});
  }
  render() {
    return (
        <div className="container login">
            <div className="col-md-6 without-social">
                <Login send={::this.logIn} />
            </div>
            <div className="col-md-6">
                <Signup send={::this.signUp} />
            </div>
        </div>
    )
  }
}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(UserActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)