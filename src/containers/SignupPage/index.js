import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as UserActions from '../../actions/UserActions'
import Signup from '../../components/Signup'


export class SignupPage extends Component {
    signUp(e) {
        e.preventDefault()
        this.props.actions.login({name: e.target.elements.signUpName.value, pass: e.target.elements.signUpPass.value});
    }
  render() {
    return (
        <div className="container">
            <Signup send={::this.signUp} />
        </div>
    )
  }
}


function mapStateToProps() {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(UserActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage)