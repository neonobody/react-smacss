import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './containers/App'
import Admin from './containers/Admin'
import LoginPage from './containers/LoginPage'
import SignupPage from './containers/SignupPage'
import NotFound from './components/NotFound'
import requireAuthentication from './containers/AuthenticatedComponent'

export const routes = (
  <div>
    <Route path='/' component={App}>
      <IndexRoute component={requireAuthentication(Admin)} />
      <Route path='/login' component={LoginPage} />
      <Route path='/signup' component={SignupPage} />
    </Route>
    <Route path='*' component={NotFound} />
  </div>
)
