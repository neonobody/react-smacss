/*eslint-disable*/
import {
    PROJECT_REQUEST,
    PROJECT_ADD,
    PROJECT_CHANGE,
    PROJECT_EDIT,
    PROJECT_REMOVE,
    INSTANCE_CREATE,
    INSTANCE_REMOVE,
    INSTANCE_EDIT
} from '../constants/Admin'

export function addProject(payload) {
    return (dispatch) => {
            dispatch({
                type: PROJECT_ADD,
                payload: {
                    name: payload.name,
                    count: payload.count
                }
            })
    }
}

export function changeProject(payload) {
    return (dispatch) => {
        dispatch({
            type: PROJECT_CHANGE,
            payload: {
                current: payload.current
            }
        })
    }
}

export function editProject(payload) {
    return (dispatch) => {
        dispatch({
            type: PROJECT_EDIT,
            payload: {
                number: payload.number,
                name: payload.name
            }
        })
    }
}

export function removeProject(payload) {
    return (dispatch) => {
        dispatch({
            type: PROJECT_REMOVE,
            payload: {
                number: payload.number
            }
        })
    }
}

export function createInstance(payload) {
    return (dispatch) => {
            dispatch({
                type: INSTANCE_CREATE,
                payload: {
                    name: payload.name,
                    size: payload.size,
                    backups: payload.backups
                }
            })
    }
}

export function removeInstance(payload) {
    return (dispatch) => {
            dispatch({
                type: INSTANCE_REMOVE,
                payload: {
                    number: payload.number
                }
            })
    }
}

export function editInstance(payload) {
    return (dispatch) => {
            dispatch({
                type: INSTANCE_EDIT,
                payload: {
                    projects: payload.instance
                }
            })
    }
}

/*eslint-enable*/
