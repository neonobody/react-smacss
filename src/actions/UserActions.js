/*eslint-disable*/
import {
  LOGIN_REQUEST,
  LOGIN_FAIL, //eslint-disable-line no-unused-vars
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS
} from '../constants/User'

import {
  ROUTING
} from '../constants/Routing'

export function login(payload) {
  return (dispatch) => {

    dispatch({
      type: LOGIN_REQUEST
    })

    setTimeout(() => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: {
          name: payload.name,
          pass: payload.pass,
          isAuthenticated: true
        }
      })

      dispatch({
        type: ROUTING,
        payload: {
          method: 'replace',
          nextUrl: '/'
        }
      })
    },2000)
  }
}

export function logout(payload) {
  return (dispatch) => {

    setTimeout(() => {
      dispatch({
        type: LOGOUT_SUCCESS,
        payload: {
          isAuthenticated: false
        }
      })

      dispatch({
        type: ROUTING,
        payload: {
          method: 'replace',
          nextUrl: '/login'
        }
      })
    },2000)

  }
}
/*eslint-enable*/
