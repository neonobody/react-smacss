import update from 'react-addons-update'
import {
    PROJECT_REQUEST,
    PROJECT_ADD,
    PROJECT_CHANGE,
    PROJECT_EDIT,
    PROJECT_REMOVE,
    INSTANCE_CREATE,
    INSTANCE_REMOVE,
    INSTANCE_EDIT
} from '../constants/Admin'

const initialState = {
    "current": 0,
    "projects" : [
        {
            "name": "project 1",
            "plan": "Free",
            "maxMemoryLimit": 64,
            "minMemoryLimit": 1,
            "instanceLimit": 4,
            "id": 0,
            "instances": [
                {
                    "name": "The Black Pearl",
                    "size": 16,
                    "backups": true,
                    "id": 1
                },
                {
                    "name": "The Black Pearl2",
                    "size": 32,
                    "backups": true,
                    "id": 2
                }
            ]
        },
        {
            "name": "project 2",
            "plan": "Free",
            "maxMemoryLimit": 128,
            "minMemoryLimit": 1,
            "instanceLimit": 6,
            "id": 1,
            "instances": [
                {
                    "name": "The White Pearl",
                    "size": 16,
                    "backups": true,
                    "id": 1
                },
                {
                    "name": "The White Pearl2",
                    "size": 32,
                    "backups": true,
                    "id": 2
                }
            ]
        }
    ]
};

export default function adminstate(state = initialState, action) {

    switch (action.type) {

        case PROJECT_REQUEST:
            // TO DO
            return state

        case PROJECT_ADD:
            return update(state, {
                projects: {
                    $push: [{
                        "name": action.payload.name,
                        "plan": "Free",
                        "maxMemoryLimit": 128,
                        "minMemoryLimit": 1,
                        "instanceLimit": 6,
                        "id": Date.now(),
                        "instances": []

                    }]
                }
            })

        case PROJECT_CHANGE:
            return {...state, current: action.payload.current}

        case PROJECT_EDIT:
            return update(state, {
                projects: {
                    [action.payload.number]:{
                        $merge: {
                            name: action.payload.name,
                            number: action.payload.number
                        }
                    }
                }
            })

        case PROJECT_REMOVE:
            return update(state, {
                projects: {$splice:
                        [[action.payload.number,1]]
                    }
                }
            )

        case INSTANCE_CREATE:
            return update(state, {
                projects: {
                    [state.current]:{
                        instances: {$push:
                            [{
                                "name": action.payload.name,
                                "size": action.payload.size,
                                "backups": action.payload.backups,
                                "id": Date.now()
                            }]
                        }
                    }
                }
            })
        case INSTANCE_REMOVE:
            return update(state, {
                projects: {
                    [state.current]:{
                        instances: {$splice:
                            [[action.payload.current,1]]
                        }
                    }
                }
            })

        case INSTANCE_EDIT:
            return {...state, instances: action.payload.instances}

        default:
            return state
    }
}
