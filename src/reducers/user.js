import {
  LOGIN_REQUEST,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS
} from '../constants/User'

const initialState = {isAuthenticated: false}

export default function userstate(state = initialState, action) {

  switch (action.type) {

    case LOGIN_REQUEST:
      // TODO
      return state

    case LOGIN_SUCCESS:
      return {...state, name: action.payload.name, pass: action.payload.pass, isAuthenticated: action.payload.isAuthenticated}

    case LOGIN_FAIL:
      // TODO
      return state

    case LOGOUT_SUCCESS:
      return {...state, isAuthenticated: action.payload.isAuthenticated}

    default:
      return state
    }
}
